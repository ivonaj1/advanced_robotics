// Generated by gencpp from file asreal_oprs/ExecuteCommandRequest.msg
// DO NOT EDIT!


#ifndef ASREAL_OPRS_MESSAGE_EXECUTECOMMANDREQUEST_H
#define ASREAL_OPRS_MESSAGE_EXECUTECOMMANDREQUEST_H


#include <string>
#include <vector>
#include <map>

#include <ros/types.h>
#include <ros/serialization.h>
#include <ros/builtin_message_traits.h>
#include <ros/message_operations.h>


namespace asreal_oprs
{
template <class ContainerAllocator>
struct ExecuteCommandRequest_
{
  typedef ExecuteCommandRequest_<ContainerAllocator> Type;

  ExecuteCommandRequest_()
    : command_name()
    , parameter_1()
    , parameter_2()  {
    }
  ExecuteCommandRequest_(const ContainerAllocator& _alloc)
    : command_name(_alloc)
    , parameter_1(_alloc)
    , parameter_2(_alloc)  {
  (void)_alloc;
    }



   typedef std::basic_string<char, std::char_traits<char>, typename ContainerAllocator::template rebind<char>::other >  _command_name_type;
  _command_name_type command_name;

   typedef std::basic_string<char, std::char_traits<char>, typename ContainerAllocator::template rebind<char>::other >  _parameter_1_type;
  _parameter_1_type parameter_1;

   typedef std::basic_string<char, std::char_traits<char>, typename ContainerAllocator::template rebind<char>::other >  _parameter_2_type;
  _parameter_2_type parameter_2;




  typedef boost::shared_ptr< ::asreal_oprs::ExecuteCommandRequest_<ContainerAllocator> > Ptr;
  typedef boost::shared_ptr< ::asreal_oprs::ExecuteCommandRequest_<ContainerAllocator> const> ConstPtr;

}; // struct ExecuteCommandRequest_

typedef ::asreal_oprs::ExecuteCommandRequest_<std::allocator<void> > ExecuteCommandRequest;

typedef boost::shared_ptr< ::asreal_oprs::ExecuteCommandRequest > ExecuteCommandRequestPtr;
typedef boost::shared_ptr< ::asreal_oprs::ExecuteCommandRequest const> ExecuteCommandRequestConstPtr;

// constants requiring out of line definition



template<typename ContainerAllocator>
std::ostream& operator<<(std::ostream& s, const ::asreal_oprs::ExecuteCommandRequest_<ContainerAllocator> & v)
{
ros::message_operations::Printer< ::asreal_oprs::ExecuteCommandRequest_<ContainerAllocator> >::stream(s, "", v);
return s;
}

} // namespace asreal_oprs

namespace ros
{
namespace message_traits
{



// BOOLTRAITS {'IsFixedSize': False, 'IsMessage': True, 'HasHeader': False}
// {'std_msgs': ['/opt/ros/kinetic/share/std_msgs/cmake/../msg']}

// !!!!!!!!!!! ['__class__', '__delattr__', '__dict__', '__doc__', '__eq__', '__format__', '__getattribute__', '__hash__', '__init__', '__module__', '__ne__', '__new__', '__reduce__', '__reduce_ex__', '__repr__', '__setattr__', '__sizeof__', '__str__', '__subclasshook__', '__weakref__', '_parsed_fields', 'constants', 'fields', 'full_name', 'has_header', 'header_present', 'names', 'package', 'parsed_fields', 'short_name', 'text', 'types']




template <class ContainerAllocator>
struct IsFixedSize< ::asreal_oprs::ExecuteCommandRequest_<ContainerAllocator> >
  : FalseType
  { };

template <class ContainerAllocator>
struct IsFixedSize< ::asreal_oprs::ExecuteCommandRequest_<ContainerAllocator> const>
  : FalseType
  { };

template <class ContainerAllocator>
struct IsMessage< ::asreal_oprs::ExecuteCommandRequest_<ContainerAllocator> >
  : TrueType
  { };

template <class ContainerAllocator>
struct IsMessage< ::asreal_oprs::ExecuteCommandRequest_<ContainerAllocator> const>
  : TrueType
  { };

template <class ContainerAllocator>
struct HasHeader< ::asreal_oprs::ExecuteCommandRequest_<ContainerAllocator> >
  : FalseType
  { };

template <class ContainerAllocator>
struct HasHeader< ::asreal_oprs::ExecuteCommandRequest_<ContainerAllocator> const>
  : FalseType
  { };


template<class ContainerAllocator>
struct MD5Sum< ::asreal_oprs::ExecuteCommandRequest_<ContainerAllocator> >
{
  static const char* value()
  {
    return "4d44685b29bb2f785222523c65f4433e";
  }

  static const char* value(const ::asreal_oprs::ExecuteCommandRequest_<ContainerAllocator>&) { return value(); }
  static const uint64_t static_value1 = 0x4d44685b29bb2f78ULL;
  static const uint64_t static_value2 = 0x5222523c65f4433eULL;
};

template<class ContainerAllocator>
struct DataType< ::asreal_oprs::ExecuteCommandRequest_<ContainerAllocator> >
{
  static const char* value()
  {
    return "asreal_oprs/ExecuteCommandRequest";
  }

  static const char* value(const ::asreal_oprs::ExecuteCommandRequest_<ContainerAllocator>&) { return value(); }
};

template<class ContainerAllocator>
struct Definition< ::asreal_oprs::ExecuteCommandRequest_<ContainerAllocator> >
{
  static const char* value()
  {
    return "string command_name\n\
string parameter_1\n\
string parameter_2\n\
";
  }

  static const char* value(const ::asreal_oprs::ExecuteCommandRequest_<ContainerAllocator>&) { return value(); }
};

} // namespace message_traits
} // namespace ros

namespace ros
{
namespace serialization
{

  template<class ContainerAllocator> struct Serializer< ::asreal_oprs::ExecuteCommandRequest_<ContainerAllocator> >
  {
    template<typename Stream, typename T> inline static void allInOne(Stream& stream, T m)
    {
      stream.next(m.command_name);
      stream.next(m.parameter_1);
      stream.next(m.parameter_2);
    }

    ROS_DECLARE_ALLINONE_SERIALIZER
  }; // struct ExecuteCommandRequest_

} // namespace serialization
} // namespace ros

namespace ros
{
namespace message_operations
{

template<class ContainerAllocator>
struct Printer< ::asreal_oprs::ExecuteCommandRequest_<ContainerAllocator> >
{
  template<typename Stream> static void stream(Stream& s, const std::string& indent, const ::asreal_oprs::ExecuteCommandRequest_<ContainerAllocator>& v)
  {
    s << indent << "command_name: ";
    Printer<std::basic_string<char, std::char_traits<char>, typename ContainerAllocator::template rebind<char>::other > >::stream(s, indent + "  ", v.command_name);
    s << indent << "parameter_1: ";
    Printer<std::basic_string<char, std::char_traits<char>, typename ContainerAllocator::template rebind<char>::other > >::stream(s, indent + "  ", v.parameter_1);
    s << indent << "parameter_2: ";
    Printer<std::basic_string<char, std::char_traits<char>, typename ContainerAllocator::template rebind<char>::other > >::stream(s, indent + "  ", v.parameter_2);
  }
};

} // namespace message_operations
} // namespace ros

#endif // ASREAL_OPRS_MESSAGE_EXECUTECOMMANDREQUEST_H
