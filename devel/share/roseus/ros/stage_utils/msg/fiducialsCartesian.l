;; Auto-generated. Do not edit!


(when (boundp 'stage_utils::fiducialsCartesian)
  (if (not (find-package "STAGE_UTILS"))
    (make-package "STAGE_UTILS"))
  (shadow 'fiducialsCartesian (find-package "STAGE_UTILS")))
(unless (find-package "STAGE_UTILS::FIDUCIALSCARTESIAN")
  (make-package "STAGE_UTILS::FIDUCIALSCARTESIAN"))

(in-package "ROS")
;;//! \htmlinclude fiducialsCartesian.msg.html
(if (not (find-package "STD_MSGS"))
  (ros::roseus-add-msgs "std_msgs"))


(defclass stage_utils::fiducialsCartesian
  :super ros::object
  :slots (_header _list ))

(defmethod stage_utils::fiducialsCartesian
  (:init
   (&key
    ((:header __header) (instance std_msgs::Header :init))
    ((:list __list) (let (r) (dotimes (i 0) (push (instance stage_utils::fiducialCartesian :init) r)) r))
    )
   (send-super :init)
   (setq _header __header)
   (setq _list __list)
   self)
  (:header
   (&rest __header)
   (if (keywordp (car __header))
       (send* _header __header)
     (progn
       (if __header (setq _header (car __header)))
       _header)))
  (:list
   (&rest __list)
   (if (keywordp (car __list))
       (send* _list __list)
     (progn
       (if __list (setq _list (car __list)))
       _list)))
  (:serialization-length
   ()
   (+
    ;; std_msgs/Header _header
    (send _header :serialization-length)
    ;; stage_utils/fiducialCartesian[] _list
    (apply #'+ (send-all _list :serialization-length)) 4
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; std_msgs/Header _header
       (send _header :serialize s)
     ;; stage_utils/fiducialCartesian[] _list
     (write-long (length _list) s)
     (dolist (elem _list)
       (send elem :serialize s)
       )
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; std_msgs/Header _header
     (send _header :deserialize buf ptr-) (incf ptr- (send _header :serialization-length))
   ;; stage_utils/fiducialCartesian[] _list
   (let (n)
     (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4)
     (setq _list (let (r) (dotimes (i n) (push (instance stage_utils::fiducialCartesian :init) r)) r))
     (dolist (elem- _list)
     (send elem- :deserialize buf ptr-) (incf ptr- (send elem- :serialization-length))
     ))
   ;;
   self)
  )

(setf (get stage_utils::fiducialsCartesian :md5sum-) "15e0fad81437089d7e39601a3b11c273")
(setf (get stage_utils::fiducialsCartesian :datatype-) "stage_utils/fiducialsCartesian")
(setf (get stage_utils::fiducialsCartesian :definition-)
      "# array of beacon in map
Header header
fiducialCartesian[] list

================================================================================
MSG: std_msgs/Header
# Standard metadata for higher-level stamped data types.
# This is generally used to communicate timestamped data 
# in a particular coordinate frame.
# 
# sequence ID: consecutively increasing ID 
uint32 seq
#Two-integer timestamp that is expressed as:
# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')
# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')
# time-handling sugar is provided by the client library
time stamp
#Frame this data is associated with
# 0: no frame
# 1: global frame
string frame_id

================================================================================
MSG: stage_utils/fiducialCartesian
# beacon in map
float64 x
float64 y
int64 id

")



(provide :stage_utils/fiducialsCartesian "15e0fad81437089d7e39601a3b11c273")


