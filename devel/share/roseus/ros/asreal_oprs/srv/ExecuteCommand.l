;; Auto-generated. Do not edit!


(when (boundp 'asreal_oprs::ExecuteCommand)
  (if (not (find-package "ASREAL_OPRS"))
    (make-package "ASREAL_OPRS"))
  (shadow 'ExecuteCommand (find-package "ASREAL_OPRS")))
(unless (find-package "ASREAL_OPRS::EXECUTECOMMAND")
  (make-package "ASREAL_OPRS::EXECUTECOMMAND"))
(unless (find-package "ASREAL_OPRS::EXECUTECOMMANDREQUEST")
  (make-package "ASREAL_OPRS::EXECUTECOMMANDREQUEST"))
(unless (find-package "ASREAL_OPRS::EXECUTECOMMANDRESPONSE")
  (make-package "ASREAL_OPRS::EXECUTECOMMANDRESPONSE"))

(in-package "ROS")





(defclass asreal_oprs::ExecuteCommandRequest
  :super ros::object
  :slots (_command_name _parameter_1 _parameter_2 ))

(defmethod asreal_oprs::ExecuteCommandRequest
  (:init
   (&key
    ((:command_name __command_name) "")
    ((:parameter_1 __parameter_1) "")
    ((:parameter_2 __parameter_2) "")
    )
   (send-super :init)
   (setq _command_name (string __command_name))
   (setq _parameter_1 (string __parameter_1))
   (setq _parameter_2 (string __parameter_2))
   self)
  (:command_name
   (&optional __command_name)
   (if __command_name (setq _command_name __command_name)) _command_name)
  (:parameter_1
   (&optional __parameter_1)
   (if __parameter_1 (setq _parameter_1 __parameter_1)) _parameter_1)
  (:parameter_2
   (&optional __parameter_2)
   (if __parameter_2 (setq _parameter_2 __parameter_2)) _parameter_2)
  (:serialization-length
   ()
   (+
    ;; string _command_name
    4 (length _command_name)
    ;; string _parameter_1
    4 (length _parameter_1)
    ;; string _parameter_2
    4 (length _parameter_2)
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; string _command_name
       (write-long (length _command_name) s) (princ _command_name s)
     ;; string _parameter_1
       (write-long (length _parameter_1) s) (princ _parameter_1 s)
     ;; string _parameter_2
       (write-long (length _parameter_2) s) (princ _parameter_2 s)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; string _command_name
     (let (n) (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4) (setq _command_name (subseq buf ptr- (+ ptr- n))) (incf ptr- n))
   ;; string _parameter_1
     (let (n) (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4) (setq _parameter_1 (subseq buf ptr- (+ ptr- n))) (incf ptr- n))
   ;; string _parameter_2
     (let (n) (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4) (setq _parameter_2 (subseq buf ptr- (+ ptr- n))) (incf ptr- n))
   ;;
   self)
  )

(defclass asreal_oprs::ExecuteCommandResponse
  :super ros::object
  :slots (_successful_execution _asrael_response ))

(defmethod asreal_oprs::ExecuteCommandResponse
  (:init
   (&key
    ((:successful_execution __successful_execution) nil)
    ((:asrael_response __asrael_response) "")
    )
   (send-super :init)
   (setq _successful_execution __successful_execution)
   (setq _asrael_response (string __asrael_response))
   self)
  (:successful_execution
   (&optional __successful_execution)
   (if __successful_execution (setq _successful_execution __successful_execution)) _successful_execution)
  (:asrael_response
   (&optional __asrael_response)
   (if __asrael_response (setq _asrael_response __asrael_response)) _asrael_response)
  (:serialization-length
   ()
   (+
    ;; bool _successful_execution
    1
    ;; string _asrael_response
    4 (length _asrael_response)
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; bool _successful_execution
       (if _successful_execution (write-byte -1 s) (write-byte 0 s))
     ;; string _asrael_response
       (write-long (length _asrael_response) s) (princ _asrael_response s)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; bool _successful_execution
     (setq _successful_execution (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;; string _asrael_response
     (let (n) (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4) (setq _asrael_response (subseq buf ptr- (+ ptr- n))) (incf ptr- n))
   ;;
   self)
  )

(defclass asreal_oprs::ExecuteCommand
  :super ros::object
  :slots ())

(setf (get asreal_oprs::ExecuteCommand :md5sum-) "a4d30e3224a3079f6b7b218c86d71650")
(setf (get asreal_oprs::ExecuteCommand :datatype-) "asreal_oprs/ExecuteCommand")
(setf (get asreal_oprs::ExecuteCommand :request) asreal_oprs::ExecuteCommandRequest)
(setf (get asreal_oprs::ExecuteCommand :response) asreal_oprs::ExecuteCommandResponse)

(defmethod asreal_oprs::ExecuteCommandRequest
  (:response () (instance asreal_oprs::ExecuteCommandResponse :init)))

(setf (get asreal_oprs::ExecuteCommandRequest :md5sum-) "a4d30e3224a3079f6b7b218c86d71650")
(setf (get asreal_oprs::ExecuteCommandRequest :datatype-) "asreal_oprs/ExecuteCommandRequest")
(setf (get asreal_oprs::ExecuteCommandRequest :definition-)
      "string command_name
string parameter_1
string parameter_2
---
bool successful_execution
string asrael_response

")

(setf (get asreal_oprs::ExecuteCommandResponse :md5sum-) "a4d30e3224a3079f6b7b218c86d71650")
(setf (get asreal_oprs::ExecuteCommandResponse :datatype-) "asreal_oprs/ExecuteCommandResponse")
(setf (get asreal_oprs::ExecuteCommandResponse :definition-)
      "string command_name
string parameter_1
string parameter_2
---
bool successful_execution
string asrael_response

")



(provide :asreal_oprs/ExecuteCommand "a4d30e3224a3079f6b7b218c86d71650")


