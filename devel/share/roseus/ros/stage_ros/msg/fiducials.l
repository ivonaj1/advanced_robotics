;; Auto-generated. Do not edit!


(when (boundp 'stage_ros::fiducials)
  (if (not (find-package "STAGE_ROS"))
    (make-package "STAGE_ROS"))
  (shadow 'fiducials (find-package "STAGE_ROS")))
(unless (find-package "STAGE_ROS::FIDUCIALS")
  (make-package "STAGE_ROS::FIDUCIALS"))

(in-package "ROS")
;;//! \htmlinclude fiducials.msg.html
(if (not (find-package "STD_MSGS"))
  (ros::roseus-add-msgs "std_msgs"))


(defclass stage_ros::fiducials
  :super ros::object
  :slots (_header _observations ))

(defmethod stage_ros::fiducials
  (:init
   (&key
    ((:header __header) (instance std_msgs::Header :init))
    ((:observations __observations) (let (r) (dotimes (i 0) (push (instance stage_ros::fiducial :init) r)) r))
    )
   (send-super :init)
   (setq _header __header)
   (setq _observations __observations)
   self)
  (:header
   (&rest __header)
   (if (keywordp (car __header))
       (send* _header __header)
     (progn
       (if __header (setq _header (car __header)))
       _header)))
  (:observations
   (&rest __observations)
   (if (keywordp (car __observations))
       (send* _observations __observations)
     (progn
       (if __observations (setq _observations (car __observations)))
       _observations)))
  (:serialization-length
   ()
   (+
    ;; std_msgs/Header _header
    (send _header :serialization-length)
    ;; stage_ros/fiducial[] _observations
    (apply #'+ (send-all _observations :serialization-length)) 4
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; std_msgs/Header _header
       (send _header :serialize s)
     ;; stage_ros/fiducial[] _observations
     (write-long (length _observations) s)
     (dolist (elem _observations)
       (send elem :serialize s)
       )
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; std_msgs/Header _header
     (send _header :deserialize buf ptr-) (incf ptr- (send _header :serialization-length))
   ;; stage_ros/fiducial[] _observations
   (let (n)
     (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4)
     (setq _observations (let (r) (dotimes (i n) (push (instance stage_ros::fiducial :init) r)) r))
     (dolist (elem- _observations)
     (send elem- :deserialize buf ptr-) (incf ptr- (send elem- :serialization-length))
     ))
   ;;
   self)
  )

(setf (get stage_ros::fiducials :md5sum-) "f3e5ad7886770f053161531e4816a8e8")
(setf (get stage_ros::fiducials :datatype-) "stage_ros/fiducials")
(setf (get stage_ros::fiducials :definition-)
      "# array of beacon observation
Header header
fiducial[] observations

================================================================================
MSG: std_msgs/Header
# Standard metadata for higher-level stamped data types.
# This is generally used to communicate timestamped data 
# in a particular coordinate frame.
# 
# sequence ID: consecutively increasing ID 
uint32 seq
#Two-integer timestamp that is expressed as:
# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')
# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')
# time-handling sugar is provided by the client library
time stamp
#Frame this data is associated with
# 0: no frame
# 1: global frame
string frame_id

================================================================================
MSG: stage_ros/fiducial
# beacon observation
float64 range
float64 bearing
int64 id

")



(provide :stage_ros/fiducials "f3e5ad7886770f053161531e4816a8e8")


