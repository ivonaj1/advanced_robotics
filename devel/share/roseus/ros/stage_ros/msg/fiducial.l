;; Auto-generated. Do not edit!


(when (boundp 'stage_ros::fiducial)
  (if (not (find-package "STAGE_ROS"))
    (make-package "STAGE_ROS"))
  (shadow 'fiducial (find-package "STAGE_ROS")))
(unless (find-package "STAGE_ROS::FIDUCIAL")
  (make-package "STAGE_ROS::FIDUCIAL"))

(in-package "ROS")
;;//! \htmlinclude fiducial.msg.html


(defclass stage_ros::fiducial
  :super ros::object
  :slots (_range _bearing _id ))

(defmethod stage_ros::fiducial
  (:init
   (&key
    ((:range __range) 0.0)
    ((:bearing __bearing) 0.0)
    ((:id __id) 0)
    )
   (send-super :init)
   (setq _range (float __range))
   (setq _bearing (float __bearing))
   (setq _id (round __id))
   self)
  (:range
   (&optional __range)
   (if __range (setq _range __range)) _range)
  (:bearing
   (&optional __bearing)
   (if __bearing (setq _bearing __bearing)) _bearing)
  (:id
   (&optional __id)
   (if __id (setq _id __id)) _id)
  (:serialization-length
   ()
   (+
    ;; float64 _range
    8
    ;; float64 _bearing
    8
    ;; int64 _id
    8
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; float64 _range
       (sys::poke _range (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;; float64 _bearing
       (sys::poke _bearing (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;; int64 _id
#+(or :alpha :irix6 :x86_64)
       (progn (sys::poke _id (send s :buffer) (send s :count) :long) (incf (stream-count s) 8))
#-(or :alpha :irix6 :x86_64)
       (cond ((and (class _id) (= (length (_id . bv)) 2)) ;; bignum
              (write-long (ash (elt (_id . bv) 0) 0) s)
              (write-long (ash (elt (_id . bv) 1) -1) s))
             ((and (class _id) (= (length (_id . bv)) 1)) ;; big1
              (write-long (elt (_id . bv) 0) s)
              (write-long (if (>= _id 0) 0 #xffffffff) s))
             (t                                         ;; integer
              (write-long _id s)(write-long (if (>= _id 0) 0 #xffffffff) s)))
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; float64 _range
     (setq _range (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;; float64 _bearing
     (setq _bearing (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;; int64 _id
#+(or :alpha :irix6 :x86_64)
      (setf _id (prog1 (sys::peek buf ptr- :long) (incf ptr- 8)))
#-(or :alpha :irix6 :x86_64)
      (setf _id (let ((b0 (prog1 (sys::peek buf ptr- :integer) (incf ptr- 4)))
                  (b1 (prog1 (sys::peek buf ptr- :integer) (incf ptr- 4))))
              (cond ((= b1 -1) b0)
                     ((and (= b1  0)
                           (<= lisp::most-negative-fixnum b0 lisp::most-positive-fixnum))
                      b0)
                    ((= b1  0) (make-instance bignum :size 1 :bv (integer-vector b0)))
                    (t (make-instance bignum :size 2 :bv (integer-vector b0 (ash b1 1)))))))
   ;;
   self)
  )

(setf (get stage_ros::fiducial :md5sum-) "5c54b6a87e23bd6ef02176a4cb4d5e96")
(setf (get stage_ros::fiducial :datatype-) "stage_ros/fiducial")
(setf (get stage_ros::fiducial :definition-)
      "# beacon observation
float64 range
float64 bearing
int64 id

")



(provide :stage_ros/fiducial "5c54b6a87e23bd6ef02176a4cb4d5e96")


