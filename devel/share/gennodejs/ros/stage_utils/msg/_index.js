
"use strict";

let fiducialsCartesian = require('./fiducialsCartesian.js');
let fiducialCartesian = require('./fiducialCartesian.js');

module.exports = {
  fiducialsCartesian: fiducialsCartesian,
  fiducialCartesian: fiducialCartesian,
};
