// Auto-generated. Do not edit!

// (in-package asreal_oprs.srv)


"use strict";

const _serializer = _ros_msg_utils.Serialize;
const _arraySerializer = _serializer.Array;
const _deserializer = _ros_msg_utils.Deserialize;
const _arrayDeserializer = _deserializer.Array;
const _finder = _ros_msg_utils.Find;
const _getByteLength = _ros_msg_utils.getByteLength;

//-----------------------------------------------------------


//-----------------------------------------------------------

class ExecuteCommandRequest {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.command_name = null;
      this.parameter_1 = null;
      this.parameter_2 = null;
    }
    else {
      if (initObj.hasOwnProperty('command_name')) {
        this.command_name = initObj.command_name
      }
      else {
        this.command_name = '';
      }
      if (initObj.hasOwnProperty('parameter_1')) {
        this.parameter_1 = initObj.parameter_1
      }
      else {
        this.parameter_1 = '';
      }
      if (initObj.hasOwnProperty('parameter_2')) {
        this.parameter_2 = initObj.parameter_2
      }
      else {
        this.parameter_2 = '';
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type ExecuteCommandRequest
    // Serialize message field [command_name]
    bufferOffset = _serializer.string(obj.command_name, buffer, bufferOffset);
    // Serialize message field [parameter_1]
    bufferOffset = _serializer.string(obj.parameter_1, buffer, bufferOffset);
    // Serialize message field [parameter_2]
    bufferOffset = _serializer.string(obj.parameter_2, buffer, bufferOffset);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type ExecuteCommandRequest
    let len;
    let data = new ExecuteCommandRequest(null);
    // Deserialize message field [command_name]
    data.command_name = _deserializer.string(buffer, bufferOffset);
    // Deserialize message field [parameter_1]
    data.parameter_1 = _deserializer.string(buffer, bufferOffset);
    // Deserialize message field [parameter_2]
    data.parameter_2 = _deserializer.string(buffer, bufferOffset);
    return data;
  }

  static getMessageSize(object) {
    let length = 0;
    length += object.command_name.length;
    length += object.parameter_1.length;
    length += object.parameter_2.length;
    return length + 12;
  }

  static datatype() {
    // Returns string type for a service object
    return 'asreal_oprs/ExecuteCommandRequest';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return '4d44685b29bb2f785222523c65f4433e';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    string command_name
    string parameter_1
    string parameter_2
    
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new ExecuteCommandRequest(null);
    if (msg.command_name !== undefined) {
      resolved.command_name = msg.command_name;
    }
    else {
      resolved.command_name = ''
    }

    if (msg.parameter_1 !== undefined) {
      resolved.parameter_1 = msg.parameter_1;
    }
    else {
      resolved.parameter_1 = ''
    }

    if (msg.parameter_2 !== undefined) {
      resolved.parameter_2 = msg.parameter_2;
    }
    else {
      resolved.parameter_2 = ''
    }

    return resolved;
    }
};

class ExecuteCommandResponse {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.successful_execution = null;
      this.asrael_response = null;
    }
    else {
      if (initObj.hasOwnProperty('successful_execution')) {
        this.successful_execution = initObj.successful_execution
      }
      else {
        this.successful_execution = false;
      }
      if (initObj.hasOwnProperty('asrael_response')) {
        this.asrael_response = initObj.asrael_response
      }
      else {
        this.asrael_response = '';
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type ExecuteCommandResponse
    // Serialize message field [successful_execution]
    bufferOffset = _serializer.bool(obj.successful_execution, buffer, bufferOffset);
    // Serialize message field [asrael_response]
    bufferOffset = _serializer.string(obj.asrael_response, buffer, bufferOffset);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type ExecuteCommandResponse
    let len;
    let data = new ExecuteCommandResponse(null);
    // Deserialize message field [successful_execution]
    data.successful_execution = _deserializer.bool(buffer, bufferOffset);
    // Deserialize message field [asrael_response]
    data.asrael_response = _deserializer.string(buffer, bufferOffset);
    return data;
  }

  static getMessageSize(object) {
    let length = 0;
    length += object.asrael_response.length;
    return length + 5;
  }

  static datatype() {
    // Returns string type for a service object
    return 'asreal_oprs/ExecuteCommandResponse';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return '1b709661d4d97718a8c8693a6631c052';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    bool successful_execution
    string asrael_response
    
    
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new ExecuteCommandResponse(null);
    if (msg.successful_execution !== undefined) {
      resolved.successful_execution = msg.successful_execution;
    }
    else {
      resolved.successful_execution = false
    }

    if (msg.asrael_response !== undefined) {
      resolved.asrael_response = msg.asrael_response;
    }
    else {
      resolved.asrael_response = ''
    }

    return resolved;
    }
};

module.exports = {
  Request: ExecuteCommandRequest,
  Response: ExecuteCommandResponse,
  md5sum() { return 'a4d30e3224a3079f6b7b218c86d71650'; },
  datatype() { return 'asreal_oprs/ExecuteCommand'; }
};
