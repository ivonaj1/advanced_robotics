// Auto-generated. Do not edit!

// (in-package stage_ros.msg)


"use strict";

const _serializer = _ros_msg_utils.Serialize;
const _arraySerializer = _serializer.Array;
const _deserializer = _ros_msg_utils.Deserialize;
const _arrayDeserializer = _deserializer.Array;
const _finder = _ros_msg_utils.Find;
const _getByteLength = _ros_msg_utils.getByteLength;
let fiducial = require('./fiducial.js');
let std_msgs = _finder('std_msgs');

//-----------------------------------------------------------

class fiducials {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.header = null;
      this.observations = null;
    }
    else {
      if (initObj.hasOwnProperty('header')) {
        this.header = initObj.header
      }
      else {
        this.header = new std_msgs.msg.Header();
      }
      if (initObj.hasOwnProperty('observations')) {
        this.observations = initObj.observations
      }
      else {
        this.observations = [];
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type fiducials
    // Serialize message field [header]
    bufferOffset = std_msgs.msg.Header.serialize(obj.header, buffer, bufferOffset);
    // Serialize message field [observations]
    // Serialize the length for message field [observations]
    bufferOffset = _serializer.uint32(obj.observations.length, buffer, bufferOffset);
    obj.observations.forEach((val) => {
      bufferOffset = fiducial.serialize(val, buffer, bufferOffset);
    });
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type fiducials
    let len;
    let data = new fiducials(null);
    // Deserialize message field [header]
    data.header = std_msgs.msg.Header.deserialize(buffer, bufferOffset);
    // Deserialize message field [observations]
    // Deserialize array length for message field [observations]
    len = _deserializer.uint32(buffer, bufferOffset);
    data.observations = new Array(len);
    for (let i = 0; i < len; ++i) {
      data.observations[i] = fiducial.deserialize(buffer, bufferOffset)
    }
    return data;
  }

  static getMessageSize(object) {
    let length = 0;
    length += std_msgs.msg.Header.getMessageSize(object.header);
    length += 24 * object.observations.length;
    return length + 4;
  }

  static datatype() {
    // Returns string type for a message object
    return 'stage_ros/fiducials';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return 'f3e5ad7886770f053161531e4816a8e8';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    # array of beacon observation
    Header header
    fiducial[] observations
    
    ================================================================================
    MSG: std_msgs/Header
    # Standard metadata for higher-level stamped data types.
    # This is generally used to communicate timestamped data 
    # in a particular coordinate frame.
    # 
    # sequence ID: consecutively increasing ID 
    uint32 seq
    #Two-integer timestamp that is expressed as:
    # * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')
    # * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')
    # time-handling sugar is provided by the client library
    time stamp
    #Frame this data is associated with
    # 0: no frame
    # 1: global frame
    string frame_id
    
    ================================================================================
    MSG: stage_ros/fiducial
    # beacon observation
    float64 range
    float64 bearing
    int64 id
    
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new fiducials(null);
    if (msg.header !== undefined) {
      resolved.header = std_msgs.msg.Header.Resolve(msg.header)
    }
    else {
      resolved.header = new std_msgs.msg.Header()
    }

    if (msg.observations !== undefined) {
      resolved.observations = new Array(msg.observations.length);
      for (let i = 0; i < resolved.observations.length; ++i) {
        resolved.observations[i] = fiducial.Resolve(msg.observations[i]);
      }
    }
    else {
      resolved.observations = []
    }

    return resolved;
    }
};

module.exports = fiducials;
