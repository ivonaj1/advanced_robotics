
"use strict";

let fiducial = require('./fiducial.js');
let fiducials = require('./fiducials.js');

module.exports = {
  fiducial: fiducial,
  fiducials: fiducials,
};
