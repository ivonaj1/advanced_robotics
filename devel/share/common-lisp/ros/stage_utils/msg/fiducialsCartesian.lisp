; Auto-generated. Do not edit!


(cl:in-package stage_utils-msg)


;//! \htmlinclude fiducialsCartesian.msg.html

(cl:defclass <fiducialsCartesian> (roslisp-msg-protocol:ros-message)
  ((header
    :reader header
    :initarg :header
    :type std_msgs-msg:Header
    :initform (cl:make-instance 'std_msgs-msg:Header))
   (list
    :reader list
    :initarg :list
    :type (cl:vector stage_utils-msg:fiducialCartesian)
   :initform (cl:make-array 0 :element-type 'stage_utils-msg:fiducialCartesian :initial-element (cl:make-instance 'stage_utils-msg:fiducialCartesian))))
)

(cl:defclass fiducialsCartesian (<fiducialsCartesian>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <fiducialsCartesian>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'fiducialsCartesian)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name stage_utils-msg:<fiducialsCartesian> is deprecated: use stage_utils-msg:fiducialsCartesian instead.")))

(cl:ensure-generic-function 'header-val :lambda-list '(m))
(cl:defmethod header-val ((m <fiducialsCartesian>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader stage_utils-msg:header-val is deprecated.  Use stage_utils-msg:header instead.")
  (header m))

(cl:ensure-generic-function 'list-val :lambda-list '(m))
(cl:defmethod list-val ((m <fiducialsCartesian>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader stage_utils-msg:list-val is deprecated.  Use stage_utils-msg:list instead.")
  (list m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <fiducialsCartesian>) ostream)
  "Serializes a message object of type '<fiducialsCartesian>"
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'header) ostream)
  (cl:let ((__ros_arr_len (cl:length (cl:slot-value msg 'list))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_arr_len) ostream))
  (cl:map cl:nil #'(cl:lambda (ele) (roslisp-msg-protocol:serialize ele ostream))
   (cl:slot-value msg 'list))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <fiducialsCartesian>) istream)
  "Deserializes a message object of type '<fiducialsCartesian>"
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'header) istream)
  (cl:let ((__ros_arr_len 0))
    (cl:setf (cl:ldb (cl:byte 8 0) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 16) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 24) __ros_arr_len) (cl:read-byte istream))
  (cl:setf (cl:slot-value msg 'list) (cl:make-array __ros_arr_len))
  (cl:let ((vals (cl:slot-value msg 'list)))
    (cl:dotimes (i __ros_arr_len)
    (cl:setf (cl:aref vals i) (cl:make-instance 'stage_utils-msg:fiducialCartesian))
  (roslisp-msg-protocol:deserialize (cl:aref vals i) istream))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<fiducialsCartesian>)))
  "Returns string type for a message object of type '<fiducialsCartesian>"
  "stage_utils/fiducialsCartesian")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'fiducialsCartesian)))
  "Returns string type for a message object of type 'fiducialsCartesian"
  "stage_utils/fiducialsCartesian")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<fiducialsCartesian>)))
  "Returns md5sum for a message object of type '<fiducialsCartesian>"
  "15e0fad81437089d7e39601a3b11c273")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'fiducialsCartesian)))
  "Returns md5sum for a message object of type 'fiducialsCartesian"
  "15e0fad81437089d7e39601a3b11c273")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<fiducialsCartesian>)))
  "Returns full string definition for message of type '<fiducialsCartesian>"
  (cl:format cl:nil "# array of beacon in map~%Header header~%fiducialCartesian[] list~%~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')~%# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%# 0: no frame~%# 1: global frame~%string frame_id~%~%================================================================================~%MSG: stage_utils/fiducialCartesian~%# beacon in map~%float64 x~%float64 y~%int64 id~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'fiducialsCartesian)))
  "Returns full string definition for message of type 'fiducialsCartesian"
  (cl:format cl:nil "# array of beacon in map~%Header header~%fiducialCartesian[] list~%~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')~%# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%# 0: no frame~%# 1: global frame~%string frame_id~%~%================================================================================~%MSG: stage_utils/fiducialCartesian~%# beacon in map~%float64 x~%float64 y~%int64 id~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <fiducialsCartesian>))
  (cl:+ 0
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'header))
     4 (cl:reduce #'cl:+ (cl:slot-value msg 'list) :key #'(cl:lambda (ele) (cl:declare (cl:ignorable ele)) (cl:+ (roslisp-msg-protocol:serialization-length ele))))
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <fiducialsCartesian>))
  "Converts a ROS message object to a list"
  (cl:list 'fiducialsCartesian
    (cl:cons ':header (header msg))
    (cl:cons ':list (list msg))
))
