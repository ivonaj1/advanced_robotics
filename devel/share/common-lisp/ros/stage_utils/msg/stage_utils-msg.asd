
(cl:in-package :asdf)

(defsystem "stage_utils-msg"
  :depends-on (:roslisp-msg-protocol :roslisp-utils :std_msgs-msg
)
  :components ((:file "_package")
    (:file "fiducialCartesian" :depends-on ("_package_fiducialCartesian"))
    (:file "_package_fiducialCartesian" :depends-on ("_package"))
    (:file "fiducialsCartesian" :depends-on ("_package_fiducialsCartesian"))
    (:file "_package_fiducialsCartesian" :depends-on ("_package"))
  ))