; Auto-generated. Do not edit!


(cl:in-package asreal_oprs-srv)


;//! \htmlinclude ExecuteCommand-request.msg.html

(cl:defclass <ExecuteCommand-request> (roslisp-msg-protocol:ros-message)
  ((command_name
    :reader command_name
    :initarg :command_name
    :type cl:string
    :initform "")
   (parameter_1
    :reader parameter_1
    :initarg :parameter_1
    :type cl:string
    :initform "")
   (parameter_2
    :reader parameter_2
    :initarg :parameter_2
    :type cl:string
    :initform ""))
)

(cl:defclass ExecuteCommand-request (<ExecuteCommand-request>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <ExecuteCommand-request>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'ExecuteCommand-request)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name asreal_oprs-srv:<ExecuteCommand-request> is deprecated: use asreal_oprs-srv:ExecuteCommand-request instead.")))

(cl:ensure-generic-function 'command_name-val :lambda-list '(m))
(cl:defmethod command_name-val ((m <ExecuteCommand-request>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader asreal_oprs-srv:command_name-val is deprecated.  Use asreal_oprs-srv:command_name instead.")
  (command_name m))

(cl:ensure-generic-function 'parameter_1-val :lambda-list '(m))
(cl:defmethod parameter_1-val ((m <ExecuteCommand-request>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader asreal_oprs-srv:parameter_1-val is deprecated.  Use asreal_oprs-srv:parameter_1 instead.")
  (parameter_1 m))

(cl:ensure-generic-function 'parameter_2-val :lambda-list '(m))
(cl:defmethod parameter_2-val ((m <ExecuteCommand-request>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader asreal_oprs-srv:parameter_2-val is deprecated.  Use asreal_oprs-srv:parameter_2 instead.")
  (parameter_2 m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <ExecuteCommand-request>) ostream)
  "Serializes a message object of type '<ExecuteCommand-request>"
  (cl:let ((__ros_str_len (cl:length (cl:slot-value msg 'command_name))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_str_len) ostream))
  (cl:map cl:nil #'(cl:lambda (c) (cl:write-byte (cl:char-code c) ostream)) (cl:slot-value msg 'command_name))
  (cl:let ((__ros_str_len (cl:length (cl:slot-value msg 'parameter_1))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_str_len) ostream))
  (cl:map cl:nil #'(cl:lambda (c) (cl:write-byte (cl:char-code c) ostream)) (cl:slot-value msg 'parameter_1))
  (cl:let ((__ros_str_len (cl:length (cl:slot-value msg 'parameter_2))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_str_len) ostream))
  (cl:map cl:nil #'(cl:lambda (c) (cl:write-byte (cl:char-code c) ostream)) (cl:slot-value msg 'parameter_2))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <ExecuteCommand-request>) istream)
  "Deserializes a message object of type '<ExecuteCommand-request>"
    (cl:let ((__ros_str_len 0))
      (cl:setf (cl:ldb (cl:byte 8 0) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'command_name) (cl:make-string __ros_str_len))
      (cl:dotimes (__ros_str_idx __ros_str_len msg)
        (cl:setf (cl:char (cl:slot-value msg 'command_name) __ros_str_idx) (cl:code-char (cl:read-byte istream)))))
    (cl:let ((__ros_str_len 0))
      (cl:setf (cl:ldb (cl:byte 8 0) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'parameter_1) (cl:make-string __ros_str_len))
      (cl:dotimes (__ros_str_idx __ros_str_len msg)
        (cl:setf (cl:char (cl:slot-value msg 'parameter_1) __ros_str_idx) (cl:code-char (cl:read-byte istream)))))
    (cl:let ((__ros_str_len 0))
      (cl:setf (cl:ldb (cl:byte 8 0) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'parameter_2) (cl:make-string __ros_str_len))
      (cl:dotimes (__ros_str_idx __ros_str_len msg)
        (cl:setf (cl:char (cl:slot-value msg 'parameter_2) __ros_str_idx) (cl:code-char (cl:read-byte istream)))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<ExecuteCommand-request>)))
  "Returns string type for a service object of type '<ExecuteCommand-request>"
  "asreal_oprs/ExecuteCommandRequest")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'ExecuteCommand-request)))
  "Returns string type for a service object of type 'ExecuteCommand-request"
  "asreal_oprs/ExecuteCommandRequest")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<ExecuteCommand-request>)))
  "Returns md5sum for a message object of type '<ExecuteCommand-request>"
  "a4d30e3224a3079f6b7b218c86d71650")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'ExecuteCommand-request)))
  "Returns md5sum for a message object of type 'ExecuteCommand-request"
  "a4d30e3224a3079f6b7b218c86d71650")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<ExecuteCommand-request>)))
  "Returns full string definition for message of type '<ExecuteCommand-request>"
  (cl:format cl:nil "string command_name~%string parameter_1~%string parameter_2~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'ExecuteCommand-request)))
  "Returns full string definition for message of type 'ExecuteCommand-request"
  (cl:format cl:nil "string command_name~%string parameter_1~%string parameter_2~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <ExecuteCommand-request>))
  (cl:+ 0
     4 (cl:length (cl:slot-value msg 'command_name))
     4 (cl:length (cl:slot-value msg 'parameter_1))
     4 (cl:length (cl:slot-value msg 'parameter_2))
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <ExecuteCommand-request>))
  "Converts a ROS message object to a list"
  (cl:list 'ExecuteCommand-request
    (cl:cons ':command_name (command_name msg))
    (cl:cons ':parameter_1 (parameter_1 msg))
    (cl:cons ':parameter_2 (parameter_2 msg))
))
;//! \htmlinclude ExecuteCommand-response.msg.html

(cl:defclass <ExecuteCommand-response> (roslisp-msg-protocol:ros-message)
  ((successful_execution
    :reader successful_execution
    :initarg :successful_execution
    :type cl:boolean
    :initform cl:nil)
   (asrael_response
    :reader asrael_response
    :initarg :asrael_response
    :type cl:string
    :initform ""))
)

(cl:defclass ExecuteCommand-response (<ExecuteCommand-response>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <ExecuteCommand-response>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'ExecuteCommand-response)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name asreal_oprs-srv:<ExecuteCommand-response> is deprecated: use asreal_oprs-srv:ExecuteCommand-response instead.")))

(cl:ensure-generic-function 'successful_execution-val :lambda-list '(m))
(cl:defmethod successful_execution-val ((m <ExecuteCommand-response>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader asreal_oprs-srv:successful_execution-val is deprecated.  Use asreal_oprs-srv:successful_execution instead.")
  (successful_execution m))

(cl:ensure-generic-function 'asrael_response-val :lambda-list '(m))
(cl:defmethod asrael_response-val ((m <ExecuteCommand-response>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader asreal_oprs-srv:asrael_response-val is deprecated.  Use asreal_oprs-srv:asrael_response instead.")
  (asrael_response m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <ExecuteCommand-response>) ostream)
  "Serializes a message object of type '<ExecuteCommand-response>"
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'successful_execution) 1 0)) ostream)
  (cl:let ((__ros_str_len (cl:length (cl:slot-value msg 'asrael_response))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_str_len) ostream))
  (cl:map cl:nil #'(cl:lambda (c) (cl:write-byte (cl:char-code c) ostream)) (cl:slot-value msg 'asrael_response))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <ExecuteCommand-response>) istream)
  "Deserializes a message object of type '<ExecuteCommand-response>"
    (cl:setf (cl:slot-value msg 'successful_execution) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:let ((__ros_str_len 0))
      (cl:setf (cl:ldb (cl:byte 8 0) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'asrael_response) (cl:make-string __ros_str_len))
      (cl:dotimes (__ros_str_idx __ros_str_len msg)
        (cl:setf (cl:char (cl:slot-value msg 'asrael_response) __ros_str_idx) (cl:code-char (cl:read-byte istream)))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<ExecuteCommand-response>)))
  "Returns string type for a service object of type '<ExecuteCommand-response>"
  "asreal_oprs/ExecuteCommandResponse")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'ExecuteCommand-response)))
  "Returns string type for a service object of type 'ExecuteCommand-response"
  "asreal_oprs/ExecuteCommandResponse")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<ExecuteCommand-response>)))
  "Returns md5sum for a message object of type '<ExecuteCommand-response>"
  "a4d30e3224a3079f6b7b218c86d71650")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'ExecuteCommand-response)))
  "Returns md5sum for a message object of type 'ExecuteCommand-response"
  "a4d30e3224a3079f6b7b218c86d71650")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<ExecuteCommand-response>)))
  "Returns full string definition for message of type '<ExecuteCommand-response>"
  (cl:format cl:nil "bool successful_execution~%string asrael_response~%~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'ExecuteCommand-response)))
  "Returns full string definition for message of type 'ExecuteCommand-response"
  (cl:format cl:nil "bool successful_execution~%string asrael_response~%~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <ExecuteCommand-response>))
  (cl:+ 0
     1
     4 (cl:length (cl:slot-value msg 'asrael_response))
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <ExecuteCommand-response>))
  "Converts a ROS message object to a list"
  (cl:list 'ExecuteCommand-response
    (cl:cons ':successful_execution (successful_execution msg))
    (cl:cons ':asrael_response (asrael_response msg))
))
(cl:defmethod roslisp-msg-protocol:service-request-type ((msg (cl:eql 'ExecuteCommand)))
  'ExecuteCommand-request)
(cl:defmethod roslisp-msg-protocol:service-response-type ((msg (cl:eql 'ExecuteCommand)))
  'ExecuteCommand-response)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'ExecuteCommand)))
  "Returns string type for a service object of type '<ExecuteCommand>"
  "asreal_oprs/ExecuteCommand")