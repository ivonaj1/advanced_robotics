(cl:in-package asreal_oprs-srv)
(cl:export '(COMMAND_NAME-VAL
          COMMAND_NAME
          PARAMETER_1-VAL
          PARAMETER_1
          PARAMETER_2-VAL
          PARAMETER_2
          SUCCESSFUL_EXECUTION-VAL
          SUCCESSFUL_EXECUTION
          ASRAEL_RESPONSE-VAL
          ASRAEL_RESPONSE
))