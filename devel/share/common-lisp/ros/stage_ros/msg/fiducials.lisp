; Auto-generated. Do not edit!


(cl:in-package stage_ros-msg)


;//! \htmlinclude fiducials.msg.html

(cl:defclass <fiducials> (roslisp-msg-protocol:ros-message)
  ((header
    :reader header
    :initarg :header
    :type std_msgs-msg:Header
    :initform (cl:make-instance 'std_msgs-msg:Header))
   (observations
    :reader observations
    :initarg :observations
    :type (cl:vector stage_ros-msg:fiducial)
   :initform (cl:make-array 0 :element-type 'stage_ros-msg:fiducial :initial-element (cl:make-instance 'stage_ros-msg:fiducial))))
)

(cl:defclass fiducials (<fiducials>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <fiducials>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'fiducials)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name stage_ros-msg:<fiducials> is deprecated: use stage_ros-msg:fiducials instead.")))

(cl:ensure-generic-function 'header-val :lambda-list '(m))
(cl:defmethod header-val ((m <fiducials>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader stage_ros-msg:header-val is deprecated.  Use stage_ros-msg:header instead.")
  (header m))

(cl:ensure-generic-function 'observations-val :lambda-list '(m))
(cl:defmethod observations-val ((m <fiducials>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader stage_ros-msg:observations-val is deprecated.  Use stage_ros-msg:observations instead.")
  (observations m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <fiducials>) ostream)
  "Serializes a message object of type '<fiducials>"
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'header) ostream)
  (cl:let ((__ros_arr_len (cl:length (cl:slot-value msg 'observations))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_arr_len) ostream))
  (cl:map cl:nil #'(cl:lambda (ele) (roslisp-msg-protocol:serialize ele ostream))
   (cl:slot-value msg 'observations))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <fiducials>) istream)
  "Deserializes a message object of type '<fiducials>"
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'header) istream)
  (cl:let ((__ros_arr_len 0))
    (cl:setf (cl:ldb (cl:byte 8 0) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 16) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 24) __ros_arr_len) (cl:read-byte istream))
  (cl:setf (cl:slot-value msg 'observations) (cl:make-array __ros_arr_len))
  (cl:let ((vals (cl:slot-value msg 'observations)))
    (cl:dotimes (i __ros_arr_len)
    (cl:setf (cl:aref vals i) (cl:make-instance 'stage_ros-msg:fiducial))
  (roslisp-msg-protocol:deserialize (cl:aref vals i) istream))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<fiducials>)))
  "Returns string type for a message object of type '<fiducials>"
  "stage_ros/fiducials")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'fiducials)))
  "Returns string type for a message object of type 'fiducials"
  "stage_ros/fiducials")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<fiducials>)))
  "Returns md5sum for a message object of type '<fiducials>"
  "f3e5ad7886770f053161531e4816a8e8")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'fiducials)))
  "Returns md5sum for a message object of type 'fiducials"
  "f3e5ad7886770f053161531e4816a8e8")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<fiducials>)))
  "Returns full string definition for message of type '<fiducials>"
  (cl:format cl:nil "# array of beacon observation~%Header header~%fiducial[] observations~%~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')~%# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%# 0: no frame~%# 1: global frame~%string frame_id~%~%================================================================================~%MSG: stage_ros/fiducial~%# beacon observation~%float64 range~%float64 bearing~%int64 id~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'fiducials)))
  "Returns full string definition for message of type 'fiducials"
  (cl:format cl:nil "# array of beacon observation~%Header header~%fiducial[] observations~%~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')~%# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%# 0: no frame~%# 1: global frame~%string frame_id~%~%================================================================================~%MSG: stage_ros/fiducial~%# beacon observation~%float64 range~%float64 bearing~%int64 id~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <fiducials>))
  (cl:+ 0
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'header))
     4 (cl:reduce #'cl:+ (cl:slot-value msg 'observations) :key #'(cl:lambda (ele) (cl:declare (cl:ignorable ele)) (cl:+ (roslisp-msg-protocol:serialization-length ele))))
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <fiducials>))
  "Converts a ROS message object to a list"
  (cl:list 'fiducials
    (cl:cons ':header (header msg))
    (cl:cons ':observations (observations msg))
))
