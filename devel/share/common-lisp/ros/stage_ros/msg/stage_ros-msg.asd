
(cl:in-package :asdf)

(defsystem "stage_ros-msg"
  :depends-on (:roslisp-msg-protocol :roslisp-utils :std_msgs-msg
)
  :components ((:file "_package")
    (:file "fiducial" :depends-on ("_package_fiducial"))
    (:file "_package_fiducial" :depends-on ("_package"))
    (:file "fiducials" :depends-on ("_package_fiducials"))
    (:file "_package_fiducials" :depends-on ("_package"))
  ))