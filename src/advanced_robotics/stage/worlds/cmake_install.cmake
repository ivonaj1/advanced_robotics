# Install script for directory: /home/ar/catkin_ws/src/advanced_robotics/stage/Stage/worlds

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/opt/ros/kinetic")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "RELEASE")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Install shared libraries without execute permission?
if(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  set(CMAKE_INSTALL_SO_NO_EXE "1")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/stage/worlds" TYPE FILE FILES
    "/home/ar/catkin_ws/src/advanced_robotics/stage/Stage/worlds/vfh.cfg"
    "/home/ar/catkin_ws/src/advanced_robotics/stage/Stage/worlds/test.cfg"
    "/home/ar/catkin_ws/src/advanced_robotics/stage/Stage/worlds/simple.cfg"
    "/home/ar/catkin_ws/src/advanced_robotics/stage/Stage/worlds/wifi.cfg"
    "/home/ar/catkin_ws/src/advanced_robotics/stage/Stage/worlds/lsp_test.cfg"
    "/home/ar/catkin_ws/src/advanced_robotics/stage/Stage/worlds/wavefront-remote.cfg"
    "/home/ar/catkin_ws/src/advanced_robotics/stage/Stage/worlds/mbicp.cfg"
    "/home/ar/catkin_ws/src/advanced_robotics/stage/Stage/worlds/amcl-sonar.cfg"
    "/home/ar/catkin_ws/src/advanced_robotics/stage/Stage/worlds/uoa_robotics_lab.cfg"
    "/home/ar/catkin_ws/src/advanced_robotics/stage/Stage/worlds/camera.cfg"
    "/home/ar/catkin_ws/src/advanced_robotics/stage/Stage/worlds/roomba.cfg"
    "/home/ar/catkin_ws/src/advanced_robotics/stage/Stage/worlds/nd.cfg"
    "/home/ar/catkin_ws/src/advanced_robotics/stage/Stage/worlds/autolab.cfg"
    "/home/ar/catkin_ws/src/advanced_robotics/stage/Stage/worlds/everything.cfg"
    "/home/ar/catkin_ws/src/advanced_robotics/stage/Stage/worlds/wavefront.cfg"
    "/home/ar/catkin_ws/src/advanced_robotics/stage/Stage/worlds/everything.world"
    "/home/ar/catkin_ws/src/advanced_robotics/stage/Stage/worlds/roomba.world"
    "/home/ar/catkin_ws/src/advanced_robotics/stage/Stage/worlds/sensor_noise_demo.world"
    "/home/ar/catkin_ws/src/advanced_robotics/stage/Stage/worlds/uoa_robotics_lab.world"
    "/home/ar/catkin_ws/src/advanced_robotics/stage/Stage/worlds/lsp_test.world"
    "/home/ar/catkin_ws/src/advanced_robotics/stage/Stage/worlds/pioneer_walle.world"
    "/home/ar/catkin_ws/src/advanced_robotics/stage/Stage/worlds/fasr_plan.world"
    "/home/ar/catkin_ws/src/advanced_robotics/stage/Stage/worlds/pioneer_flocking.world"
    "/home/ar/catkin_ws/src/advanced_robotics/stage/Stage/worlds/mbicp.world"
    "/home/ar/catkin_ws/src/advanced_robotics/stage/Stage/worlds/sensor_noise_module_demo.world"
    "/home/ar/catkin_ws/src/advanced_robotics/stage/Stage/worlds/SFU.world"
    "/home/ar/catkin_ws/src/advanced_robotics/stage/Stage/worlds/fasr.world"
    "/home/ar/catkin_ws/src/advanced_robotics/stage/Stage/worlds/simple.world"
    "/home/ar/catkin_ws/src/advanced_robotics/stage/Stage/worlds/large.world"
    "/home/ar/catkin_ws/src/advanced_robotics/stage/Stage/worlds/autolab.world"
    "/home/ar/catkin_ws/src/advanced_robotics/stage/Stage/worlds/wifi.world"
    "/home/ar/catkin_ws/src/advanced_robotics/stage/Stage/worlds/fasr2.world"
    "/home/ar/catkin_ws/src/advanced_robotics/stage/Stage/worlds/camera.world"
    "/home/ar/catkin_ws/src/advanced_robotics/stage/Stage/worlds/hokuyo.inc"
    "/home/ar/catkin_ws/src/advanced_robotics/stage/Stage/worlds/beacons.inc"
    "/home/ar/catkin_ws/src/advanced_robotics/stage/Stage/worlds/pantilt.inc"
    "/home/ar/catkin_ws/src/advanced_robotics/stage/Stage/worlds/objects.inc"
    "/home/ar/catkin_ws/src/advanced_robotics/stage/Stage/worlds/walle.inc"
    "/home/ar/catkin_ws/src/advanced_robotics/stage/Stage/worlds/uoa_robotics_lab_models.inc"
    "/home/ar/catkin_ws/src/advanced_robotics/stage/Stage/worlds/map.inc"
    "/home/ar/catkin_ws/src/advanced_robotics/stage/Stage/worlds/chatterbox.inc"
    "/home/ar/catkin_ws/src/advanced_robotics/stage/Stage/worlds/irobot.inc"
    "/home/ar/catkin_ws/src/advanced_robotics/stage/Stage/worlds/ubot.inc"
    "/home/ar/catkin_ws/src/advanced_robotics/stage/Stage/worlds/sick.inc"
    "/home/ar/catkin_ws/src/advanced_robotics/stage/Stage/worlds/pioneer.inc"
    "/home/ar/catkin_ws/src/advanced_robotics/stage/Stage/worlds/worldgen.sh"
    "/home/ar/catkin_ws/src/advanced_robotics/stage/Stage/worlds/cfggen.sh"
    "/home/ar/catkin_ws/src/advanced_robotics/stage/Stage/worlds/test.sh"
    )
endif()

if(NOT CMAKE_INSTALL_LOCAL_ONLY)
  # Include the install script for each subdirectory.
  include("/home/ar/catkin_ws/src/advanced_robotics/stage/worlds/benchmark/cmake_install.cmake")
  include("/home/ar/catkin_ws/src/advanced_robotics/stage/worlds/bitmaps/cmake_install.cmake")
  include("/home/ar/catkin_ws/src/advanced_robotics/stage/worlds/wifi/cmake_install.cmake")

endif()

