# CMake generated Testfile for 
# Source directory: /home/ar/catkin_ws/src/advanced_robotics/stage/Stage/worlds
# Build directory: /home/ar/catkin_ws/src/advanced_robotics/stage/worlds
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs(benchmark)
subdirs(bitmaps)
subdirs(wifi)
