# CMake generated Testfile for 
# Source directory: /home/ar/catkin_ws/src/advanced_robotics/stage/Stage
# Build directory: /home/ar/catkin_ws/src/advanced_robotics/stage
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs(libstage)
subdirs(examples)
subdirs(assets)
subdirs(worlds)
