# generated from catkin/cmake/template/pkg.context.pc.in
CATKIN_PACKAGE_PREFIX = ""
PROJECT_PKG_CONFIG_INCLUDE_DIRS = "/home/ar/catkin_ws/devel/include;/opt/ros/kinetic/include/Stage-4.1;/usr/lib/fltk;/usr/include".split(';') if "/home/ar/catkin_ws/devel/include;/opt/ros/kinetic/include/Stage-4.1;/usr/lib/fltk;/usr/include" != "" else []
PROJECT_CATKIN_DEPENDS = "geometry_msgs;nav_msgs;roscpp;sensor_msgs;std_msgs;tf;message_runtime".replace(';', ' ')
PKG_CONFIG_LIBRARIES_WITH_PREFIX = "-lstage_ros;-l:/opt/ros/kinetic/lib/libstage.so;-l:/usr/lib/x86_64-linux-gnu/libboost_system.so;-l:/usr/lib/x86_64-linux-gnu/libboost_thread.so;-l:/usr/lib/x86_64-linux-gnu/libboost_chrono.so;-l:/usr/lib/x86_64-linux-gnu/libboost_date_time.so;-l:/usr/lib/x86_64-linux-gnu/libboost_atomic.so;-l:/usr/lib/x86_64-linux-gnu/libpthread.so".split(';') if "-lstage_ros;-l:/opt/ros/kinetic/lib/libstage.so;-l:/usr/lib/x86_64-linux-gnu/libboost_system.so;-l:/usr/lib/x86_64-linux-gnu/libboost_thread.so;-l:/usr/lib/x86_64-linux-gnu/libboost_chrono.so;-l:/usr/lib/x86_64-linux-gnu/libboost_date_time.so;-l:/usr/lib/x86_64-linux-gnu/libboost_atomic.so;-l:/usr/lib/x86_64-linux-gnu/libpthread.so" != "" else []
PROJECT_NAME = "stage_ros"
PROJECT_SPACE_DIR = "/home/ar/catkin_ws/devel"
PROJECT_VERSION = "1.7.2"
