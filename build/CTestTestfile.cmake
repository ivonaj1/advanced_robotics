# CMake generated Testfile for 
# Source directory: /home/ar/catkin_ws/src
# Build directory: /home/ar/catkin_ws/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs(gtest)
subdirs(advanced_robotics/asreal_oprs)
subdirs(advanced_robotics/occupancy_grid_utils)
subdirs(advanced_robotics/laser_based_localization_pf)
subdirs(advanced_robotics/stage_ros)
subdirs(advanced_robotics/stage_utils)
subdirs(advanced_robotics/g2o_based_mapping)
subdirs(advanced_robotics/lpn_global_path_planner)
